import websocket
import time
import json
import random

if __name__ == "__main__":
    # Config
    # Total Collection Time
    Total_Run = 60 * 5

    # Interval of receiving data
    Cmd_Check_Interval = 1

    # Timestamp
    start = time.time()
    tick = time.time()

    # WebSocket
    ws = websocket.WebSocket()
    ws.connect("wss://stream.binance.com:9443/ws/btcusdt@depth20")

    # Records
    pre_record = None

    def cmp(cur, pre, side):
        output = []
        if cur is None or pre is None or len(cur) == 0 or len(pre) == 0:
            return ""

        timestamp = time.time()
        for cur_obj in cur:
            cur_in_pre = False
            for pre_obj in pre:
                if cur_obj[0] == pre_obj[0]:
                    cur_in_pre = True
            if cur_in_pre == False:
                output.append(str(timestamp) + ',' + 'add' + ',' + str(cur_obj[0]) + ',' + str(cur_obj[1]) + ',' + str(side) + '\n')
        print('tick ...')
        for pre_obj in pre:
            pre_in_cur = False
            for cur_obj in cur:
                if pre_obj[0] == cur_obj[0]:
                    pre_in_cur = True
            if pre_in_cur == False:
                output.append(str(timestamp) + ',' + 'del' + ',' + str(pre_obj[0]) + ',' + str(pre_obj[1]) + ',' + str(side) + '\n')


        return "".join(output)

    # CSV File open, w - clean previous content when we open
    f = open('btc_usdt_log.csv', 'w')

    while (time.time() - start) < Total_Run:
        if (time.time() - tick) >= Cmd_Check_Interval:
            tick = time.time()
            payload = ws.recv()
            try:
                cur_record = json.loads(payload)

                if pre_record is None:
                    pre_record = cur_record
                    continue

                output = cmp(cur_record['bids'], pre_record['bids'], 'bids')
                if len(output):
                    f.write(output)
                output = cmp(cur_record['asks'], pre_record['asks'], 'asks')
                if len(output):
                    f.write(output)

                pre_record = cur_record
            except Exception as e:
                print("[JSON Exception] - ", e)

    # CSV File close
    f.close()

    print('end')
